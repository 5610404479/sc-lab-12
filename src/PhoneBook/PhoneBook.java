package PhoneBook;

public class PhoneBook {
	private String name ;
	private String callnumber ;
	
	public PhoneBook(String name, String callnumber){
		this.setName(name);
		this.setCallnumber(callnumber);
	}

	public String getCallnumber() {
		return callnumber;
	}

	public void setCallnumber(String callnumber) {
		this.callnumber = callnumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
