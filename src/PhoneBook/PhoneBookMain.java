package PhoneBook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PhoneBookMain {
	PhonebookGUI test;

	public static void main(String args[]) throws Exception {

		new PhoneBookMain();
	}

	public PhoneBookMain() throws IOException {
		test = new PhonebookGUI();
		test.pack();
		test.setVisible(true);
		test.setSize(600, 1000);
		BufferedReader br = new BufferedReader(new FileReader("phonebook.txt"));
		ArrayList<PhoneBook> phonelist = new ArrayList<PhoneBook>();
		String line = br.readLine();

		while (line != null) {
			String[] data = line.split(",");
			String name = data[0].trim();
			String phonenumber = data[1].trim();
			PhoneBook p = new PhoneBook(name, phonenumber);
			phonelist.add(p);
			line = br.readLine();

		}
		test.setResult("--------- Java Phone Book ---------" + "\n"
				+ "Name Phone" + "\n" + "==== =====");
		for(PhoneBook i :phonelist){
			test.extendResult(i.getName()+"\t"+i.getCallnumber());
			
		}
	}
}
