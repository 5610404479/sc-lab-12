package HomeworkAndExamAVG;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import PhoneBook.PhoneBook;

public class StudentMain {

	public static void main(String[] args) throws IOException {
		FileWriter fileWriter = null;

		BufferedReader br = new BufferedReader(new FileReader("homework.txt"));
		ArrayList<Student> Stdlist = new ArrayList<Student>();
		String line = br.readLine();

		while (line != null) {
			String[] data = line.split(",");
			String name = data[0].trim();
			double s1 = Double.parseDouble(data[1].trim());
			double s2 = Double.parseDouble(data[2].trim());
			double s3 = Double.parseDouble(data[3].trim());
			double s4 = Double.parseDouble(data[4].trim());
			double s5 = Double.parseDouble(data[5].trim());
			Student s = new Student(name, s1, s2, s3, s4, s5);
			Stdlist.add(s);
			line = br.readLine();

		}
		BufferedReader br2 = new BufferedReader(new FileReader("exam.txt"));
		ArrayList<Student> examlist = new ArrayList<Student>();
		String line2 = br2.readLine();
		while (line2 != null) {
			String[] data = line2.split(",");
			String name = data[0].trim();
			double s1 = Double.parseDouble(data[1].trim());
			double s2 = Double.parseDouble(data[2].trim());

			Student examStd = new Student(name, s1, s2);
			examlist.add(examStd);
			line2 = br2.readLine();

		}

		fileWriter = new FileWriter("average.txt");
		BufferedWriter out = new BufferedWriter(fileWriter);
		out.write("--------- Homework Scores ---------" + "\n" + "Name Average"
				+ "\n" + "==== =======");
		out.newLine();
		for (Student i : Stdlist) {

			out.write(i.getName() + "\t" + i.getavg());
			out.newLine();

		}
		out.write("--------- Exam Scores ---------" + "\n" + "Name Average"
				+ "\n" + "==== =======");
		out.newLine();
		for (Student i : examlist) {

			out.write(i.getName() + "\t" + i.getExamscoreAVG());
			out.newLine();

		}
		out.flush();
	}
}
