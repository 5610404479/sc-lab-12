package part2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class HomeWorkScore {

	public void read() throws IOException {
		String line;
		Double score = 0.0;
		Double sum = 0.0;

		String filename = "homework.txt";
		FileReader fileReader = new FileReader(filename);
		BufferedReader buffer = new BufferedReader(fileReader);

		System.out.print("----------HomeWork Score-----------\n");
		System.out.println("\t"+"\t"+"   Name"+"    "+"Average Score");
		System.out.println("\t"+"\t"+"   ===="+"    "+"===========");
		
		FileWriter fileWriter = new FileWriter("average.txt");
		PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));

		for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
			String[] list = line.split(",");

			for (int i = 1; i < list.length; i++) {
				score = Double.parseDouble(list[i]);
				sum += score;
			}

			String name = list[0];
			out.println("\t" + name + "\t" + sum / (list.length - 1));
			out.flush();
			System.out.println("\t" + name + "\t" + sum / (list.length - 1));
			sum = 0.0;
		}

	}
}
