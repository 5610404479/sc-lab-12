package part1;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class PhoneBook {

	public void read() throws IOException {
		String filename = "phonebook.txt";
		FileReader fileReader = null;
		fileReader = new FileReader(filename);
		BufferedReader buf = new BufferedReader(fileReader);
		String line;
		System.out.print("----------Phone Book-----------\n");
		System.out.println("\t"+"Name, Number\n");
		for (line = buf.readLine(); line != null; line = buf.readLine()) {
			System.out.println("\t" + line);

		}
	}

	public void addNumber() throws IOException {
		FileWriter file = new FileWriter("phonebook.txt", true);
		InputStreamReader inReader = new InputStreamReader(System.in);
		BufferedReader buffer = new BufferedReader(inReader); 
		PrintWriter out = new PrintWriter(new BufferedWriter(file));
		System.out.println("\nInput Name, Number >>>>>");
		String line = buffer.readLine();
		while (!line.equals("bye")) {;
			out.println(line);
			line = buffer.readLine();
		}
		out.flush();

	}
}
